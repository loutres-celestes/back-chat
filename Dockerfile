FROM python:3.6

ADD app.py ./

CMD ["python", "app.py"]